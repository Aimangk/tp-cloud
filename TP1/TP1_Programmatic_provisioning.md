
# TP1 : Programmatic provisioning

## Sommaire

- [TP1 : Programmatic provisioning](#tp1--programmatic-provisioning)
  - [Sommaire](#sommaire)
- [I. Une première VM](#i-une-première-vm)
  - [1. Un peu de conf](#2-un-peu-de-conf)
- [II. Initialization script](#ii-initialization-script)
- [III. Repackaging](#iii-repackaging)
- [IV. Multi VM](#iv-multi-vm)
- [V. cloud-init](#v-cloud-init)


# I. Une première VM

🌞 **`Vagrantfiles`**   
- 1.<a href="https://gitlab.com/Aimangk/tp-cloud/-/blob/main/TP1/rendu_vagrantfile/1-Vagrantfile/Vagrantfile?ref_type=heads" target="_blank">Vagrantfile première conf</a>
- 2.<a href="https://gitlab.com/Aimangk/tp-cloud/-/blob/main/TP1/rendu_vagrantfile/2-Vagrantfile/Vagrantfile?ref_type=heads" target="_blank">Vagrantfile script conf</a>
- 3.<a href="https://gitlab.com/Aimangk/tp-cloud/-/blob/main/TP1/rendu_vagrantfile/3-Vagrantfile/Vagrantfile?ref_type=heads" target="_blank">Vagrantfile multi VM</a>
- 4.<a href="https://gitlab.com/Aimangk/tp-cloud/-/blob/main/TP1/rendu_vagrantfile/4-Vagrantfile/Vagrantfile?ref_type=heads" target="_blank">Vagrantfile cloud-init</a>

## 1. Un peu de conf

🌞 **Ajustez le `Vagrantfile` pour que la VM créée** :
```vagrantfile
Vagrant.configure("2") do |config|

  config.vm.box = "generic/rocky9"

  config.vm.hostname = "ezconf.tp1.efrei"
  config.vm.define "ezconf.tp1.efrei"
  config.vm.network :private_network, ip: "10.1.1.11", netmask: "255.255.255.0"
  config.vm.disk :disk, name: "backup", size: "20GB"
  

  
  config.vm.provider :virtualbox do |v| 

    

    v.memory = 2048

    

    

  end 

  
end

```

# II. Initialization script


🌞 **Ajustez le `Vagrantfile`** :

```vagrantfile
Vagrant.configure("2") do |config|

  config.vm.box = "generic/rocky9"

  config.vm.hostname = "ezconf.tp1.efrei"
  config.vm.define "ezconf.tp1.efrei"
  config.vm.network :private_network, ip: "10.1.1.11", netmask: "255.255.255.0"
  config.vm.disk :disk, name: "backup", size: "20GB"
  config.vm.provision "shell", path: "script.sh"
  
  config.vm.provider :virtualbox do |v| 

    

    v.memory = 2048

    

    

  end 

  
end

```
- script    
```bash
#!/bin/bash
sudo dnf update -y
sudo dnf install -y vim python3;
```

# III. Repackaging

🌞 **Repackager la VM créée précédemment**
```
$ vagrant package --output rocky-efrei.box

$ vagrant box add rocky-efrei rocky-efrei.box
```
```
vagrant box repackage rocky-efrei.box virtualbox 1.0
```

# IV. Multi VM

🌞 **Un deuxième `Vagrantfile` qui définit** :

```
Vagrant.configure("2") do |config|

  
  vms = [
    {  ram: "2048" },
    {  ram: "1024" }
  ]

 
  vms.each_with_index do |vm, i|
    config.vm.define "node#{i+1}" do |node|
      node.vm.box = "generic/rocky9"
      node.vm.hostname = "node#{i+1}.tp1.efrei"
      
      node.vm.network "private_network", ip: "10.1.1.10#{i+1}"
      
      node.vm.provider "virtualbox" do |vb|
        vb.memory = vm[:ram]
      end
    end
  end

end

```

🌞 **Une fois les VMs allumées, assurez-vous que vous pouvez ping `10.1.1.102` depuis `node1`**
```gitbash
0bots@Caram MINGW64 ~/Documents/AimanG/Cours/Cloud/TP1/rocky9 (master)
$ vagrant ssh node1
Last login: Thu Apr  4 13:48:17 2024 from 10.0.2.2
[vagrant@node1 ~]$ ping 10.1.1.102
PING 10.1.1.102 (10.1.1.102) 56(84) bytes of data.
64 bytes from 10.1.1.102: icmp_seq=1 ttl=64 time=1.33 ms
64 bytes from 10.1.1.102: icmp_seq=2 ttl=64 time=1.34 ms



```
# V. cloud-init

🌞 **Repackager une box Vagrant**
- vagrantfile
```
Vagrant.configure("2") do |config|

  config.vm.box = "CloudInit"
  config.vm.hostname = "VmCloudinit"
  config.vm.define "VmCloudinit"
  config.vm.provision "shell", path: "script.sh"
  config.vm.cloud_init :user_data, content_type: "text/cloud-config", path: "user_data.yml"
  
end
```
- script
```
#!/bin/bash
sudo dnf install -y cloud-init
systemctl enable cloud-init
```

- doc .yml
```
---
  users:
    - name: lala
      gecos: Super adminsys
      primary_group: lala
      groups: wheel
      shell: /bin/bash
      sudo: ALL=(ALL) NOPASSWD:ALL
      lock_passwd: false
      passwd: $6$bV62paDqH/ZQSVFb$jiBgcgpkuzmmoZSvvLPwpd4gjwvnKQEWTE119tMNTnICtMcJ6dyPcDCVaTur8j5UQFuxAAM6eTimGdr97Nagh1
      ssh_authorized_keys:
        - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIODdT1gs6VBizSeKArSROeZ/NB/j0qJYwGvXPIzRsc/J 0bots@Caram

```


🌞 **Tester !**
```gitbash
0bots@Caram MINGW64 ~/Documents/AimanG/Cours/Cloud/TP1/rocky9 (master)
$ ssh lala@127.0.0.1 -p 2222
The authenticity of host '[127.0.0.1]:2222 ([127.0.0.1]:2222)' can't be established.
ED25519 key fingerprint is SHA256:MZWD7pim1ol/W9plL5CV9olEIP/EmHWZoGDcW+a9gv4.
This key is not known by any other names.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '[127.0.0.1]:2222' (ED25519) to the list of known hosts.
Last login: Thu Apr  4 16:32:31 2024
[lala@VmCloudinit ~]$

```

