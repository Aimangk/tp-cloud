# TP2 : Network boot

## Sommaire

- [TP2 : Network boot](#tp2--network-boot)
  - [Sommaire](#sommaire)
- [I. Installation d'un serveur DHCP](#i-installation-dun-serveur-dhcp)
- [II. Installation d'un serveur TFTP](#ii-installation-dun-serveur-tftp)
- [IV. Installation d'un serveur Apache](#iv-installation-dun-serveur-apache)
- [V. Test](#v-test)


# I. Installation d'un serveur DHCP

🌞 **Installer le paquet `dhcp-server`**
```bash
 servPXE: ================================================================================
    servPXE: Installing:
    servPXE:  dhcp-server           x86_64     12:4.4.2-19.b1.el9        baseos        1.2 M
    servPXE:  httpd                 x86_64     2.4.57-5.el9              appstream      46 k
    servPXE:  tftp-server           x86_64     5.2-37.el9                appstream      40 k
```
🌞 **Configurer le serveur DHCP**
```bash
[vagrant@servPXE ~]$ sudo cat /etc/dhcp/dhcpd.conf
default-lease-time 600;
max-lease-time 7200;
authoritative;

# PXE specifics
option space pxelinux;
option pxelinux.magic code 208 = string;
option pxelinux.configfile code 209 = text;
option pxelinux.pathprefix code 210 = text;
option pxelinux.reboottime code 211 = unsigned integer 32;
option architecture-type code 93 = unsigned integer 16;

subnet 10.0.0.0 netmask 255.255.255.0 {
    # définition de la range pour que votre DHCP attribue des IP entre <FIRST_IP> <LAST_IP>
    range dynamic-bootp 10.0.0.2 10.0.0.5;

    # add follows
    class "pxeclients" {
        match if substring (option vendor-class-identifier, 0, 9) = "PXEClient";
        next-server 10.0.0.10;

        if option architecture-type = 00:07 {
            filename "BOOTX64.EFI";
        }
        else {
            filename "pxelinux.0";
        }
    }
}
```

🌞 **Démarrer le serveur DHCP**
```bash
[vagrant@servPXE ~]$ sudo systemctl start dhcpd
[vagrant@servPXE ~]$ systemctl enable dhcpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: root
Password: 
==== AUTHENTICATION COMPLETE ====
Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service.
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: root
Password: 
==== AUTHENTICATION COMPLETE ====
```
🌞 **Ouvrir le bon port firewall**
```bash
[vagrant@servPXE ~]$ sudo firewall-cmd --add-service=dhcp --permanent
success
[vagrant@servPXE ~]$ sudo firewall-cmd --reload
success
```

# II. Installation d'un serveur TFTP


🌞 **Installer le paquet `tftp-server`**
```bash
 servPXE: ================================================================================
    servPXE: Installing:
    servPXE:  dhcp-server           x86_64     12:4.4.2-19.b1.el9        baseos        1.2 M
    servPXE:  httpd                 x86_64     2.4.57-5.el9              appstream      46 k
    servPXE:  tftp-server           x86_64     5.2-37.el9                appstream      40 k
```

🌞 **Démarrer le socket TFTP**
```bash
[vagrant@servPXE ~]$ sudo systemctl enable --now tftp.socket
Created symlink /etc/systemd/system/sockets.target.wants/tftp.socket → /usr/lib/systemd/system/tftp.socket.
```

🌞 **Ouvrir le bon port firewall**
```bash
[vagrant@servPXE ~]$ sudo firewall-cmd --add-service=tftp --permanent
success
[vagrant@servPXE ~]$ sudo firewall-cmd --reload
success
```

# IV. Installation d'un serveur Apache

🌞 **Installer le paquet `httpd`**
```bash
 servPXE: ================================================================================
    servPXE: Installing:
    servPXE:  dhcp-server           x86_64     12:4.4.2-19.b1.el9        baseos        1.2 M
    servPXE:  httpd                 x86_64     2.4.57-5.el9              appstream      46 k
    servPXE:  tftp-server           x86_64     5.2-37.el9                appstream      40 k
```

🌞 **Ajouter un fichier de conf dans `/etc/httpd/conf.d/pxeboot.conf`**
```bash
Alias /rocky9 /var/pxe/rocky9
<Directory /var/pxe/rocky9>
    Options Indexes FollowSymLinks
    # access permission
    Require ip 127.0.0.1 10.0.0.0/24 
</Directory>
```
🌞 **Démarrer le serveur Apache**
```bash
[vagrant@servPXE ~]$ sudo systemctl start httpd
```
🌞 **Ouvrir le bon port firewall**
```bash
[vagrant@servPXE ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[vagrant@servPXE ~]$ sudo firewall-cmd --reload
success
```

# V. Test

🌞 **Analyser l'échange complet avec Wireshark**

```bash
21:39:59.763385 IP _gateway.49679 > servPXE.ssh: Flags [.], ack 8908, win 65535, length 0
21:39:59.767767 IP servPXE.38135 > ntp1.omdc.pl.ntp: NTPv4, Client, length 48
21:39:59.800357 IP ntp1.omdc.pl.ntp > servPXE.38135: NTPv4, Server, length 48
21:39:59.863005 IP servPXE.36047 > 10.0.2.3.domain: 37989+ PTR? 115.117.222.92.in-addr.arpa. (45)
21:39:59.863636 IP servPXE.ssh > _gateway.49679: Flags [P.], seq 8908:8976, ack 1, win 62780, length 68   
21:39:59.864396 IP _gateway.49679 > servPXE.ssh: Flags [.], ack 8976, win 65535, length 0
21:39:59.913737 IP 10.0.2.3.domain > servPXE.36047: 37989 1/0/0 PTR ntp1.omdc.pl. (71)
```